FROM linuxserver/webtop:ubuntu-mate-version-7da9d194
ENV PUID=1000
ENV PGID=1000
ENV TZ=Etc/UTC

ENV CUSTOM_USER="abc"
RUN usermod -aG sudo $CUSTOM_USER
ENV TITLE="🐈‍⬛🐈‍⬛🐈‍⬛🐈‍⬛🐈‍⬛🐈‍⬛🐈‍⬛"


RUN echo "**************install utils****************"
RUN sudo apt update 
RUN sudo apt install -y nano wget git  dconf-cli caja-open-terminal 
RUN npm install -g web-ext


RUN echo "**************unsudo docker dind *******************"
RUN sudo usermod -aG docker $CUSTOM_USER


RUN echo "**************set terminal theme *******************"
RUN /usr/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/ohmybash/oh-my-bash/master/tools/install.sh)" --prefix=/usr/local
RUN cp /usr/local/share/oh-my-bash/bashrc ~/.bashrc
RUN sed -i 's/OSH_THEME="[^"]*"/OSH_THEME="pure"/' ~/.bashrc
RUN echo "**************Load Default os Data *******************"
RUN gsettings set org.gnome.desktop.screensaver ubuntu-lock-on-suspend true
ADD startwm.sh  /defaults/startwm.sh
ADD mate-screensaver-command.desktop  /usr/share/mate/autostart/mate-screensaver-command.desktop
RUN git clone https://gitlab.com/MhadhbiXissam/public-bochka-osx.git /tmp/public-bochka-osx 
RUN cp /tmp/public-bochka-osx/dconf-settings.ini  /config/dconf-settings.ini
RUN cp /tmp/public-bochka-osx/osx.gitignore  /config/.gitignore
RUN rm -rf /tmp/public-bochka-osx
RUN echo "/usr/bin/dconf load / < /config/dconf-settings.ini"  | cat - /defaults/startwm.sh > temp && mv temp /defaults/startwm.sh
RUN echo "sudo chown -R $CUSTOM_USER: /config && sudo  chmod -R u+rw /config"  | cat - /defaults/startwm.sh > temp && mv temp /defaults/startwm.sh

RUN echo "**************install conda****************"
RUN wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh -O miniconda.sh
RUN sudo chmod +x miniconda.sh && ./miniconda.sh -b -p /miniconda
RUN rm miniconda.sh
ENV PATH="/miniconda/bin:${PATH}"
RUN /miniconda/bin/conda config --set auto_activate_base true

RUN echo "**************install sublime text****************"
RUN wget -qO - https://download.sublimetext.com/sublimehq-pub.gpg | gpg --dearmor | sudo tee /etc/apt/trusted.gpg.d/sublimehq-archive.gpg > /dev/null
RUN echo "deb https://download.sublimetext.com/ apt/stable/" | sudo tee /etc/apt/sources.list.d/sublime-text.list
RUN sudo apt-get update && sudo apt-get install sublime-text


RUN echo "**************install vscode  ****************"
# Import Microsoft GPG key
RUN curl -sSL https://packages.microsoft.com/keys/microsoft.asc | apt-key add -

# Add the Visual Studio Code repository
RUN add-apt-repository "deb [arch=amd64] https://packages.microsoft.com/repos/vscode stable main"

# Install Visual Studio Code
RUN apt-get update && apt-get install -y code

# Cleanup
RUN apt-get clean && apt-get autoclean && apt-get autoremove -y && rm -rf /var/lib/apt/lists/*






RUN echo "**************configure git ****************"
RUN git config --global credential.helper 'store --file ~/.git-credentials'
RUN cd /config && git init . && git init . && git config --global init.defaultBranch main && git branch -m main
